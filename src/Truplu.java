
public class Truplu {
	String numeDoc1;
	String numeDoc2;
	double sim;
	
	public Truplu(String doc1,String doc2,double similitudiny){
		this.numeDoc1 = doc1;
		this.numeDoc2 = doc2;
		this.sim =similitudiny;
	}
	
	public String toString(){
		String s = "";
		s = (this.numeDoc1 + ";" + this.numeDoc2 + ";" + sim+"\n" );
		return s;
	}
}
