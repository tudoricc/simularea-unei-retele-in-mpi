package solutions;

import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

public class ComparePartialSolution {
/*
 * nume document 1 si hash-ul cu numarul de aparitii al cuvintelor pentru documentul 1
 * nume document 2 si hash-ul cu numarul de aparitii al cuvintelor pentru documentul 2
 * 
 */
	public String nameDocument1;
	public ConcurrentHashMap<String , Integer> totalOccurences1 ; 
	public String nameDocument2;
	public ConcurrentHashMap<String , Integer> totalOccurences2 ; 
	
	public ComparePartialSolution(String docname1 ,ConcurrentHashMap<String , Integer> resultConcatReduce1,String docname2 ,ConcurrentHashMap<String , Integer> resultConcatReduce2){
		this.nameDocument1 = docname1;
		this.nameDocument2 = docname2;
		this.totalOccurences1 = resultConcatReduce1;
		this.totalOccurences2 = resultConcatReduce2;
	}
	
	public String toString(){
		String ceva = "DocName1 : \n" + this.nameDocument1 + "\nHashTable :\n" + this.totalOccurences1 + "\n";
		ceva += "Doc Name 2: \n" + this.nameDocument2 + "\nHashTable2 : \n" + this.totalOccurences2 +"\n";
		return ceva;
	}
	
	
}
